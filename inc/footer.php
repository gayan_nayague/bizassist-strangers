</div>
</body>
<!--   Core JS Files   -->
<script src="<?php echo $baseUrl; ?>/assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo $baseUrl; ?>/assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--data table-->
<script src="<?php echo $baseUrl; ?>/assets/js/jquery.dataTables.min.js"></script>
<!--data table-->

<script src="<?php echo $baseUrl; ?>/assets/js/material.min.js" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="<?php echo $baseUrl; ?>/assets/js/chartist.min.js"></script>
<!--  Dynamic Elements plugin -->
<script src="<?php echo $baseUrl; ?>/assets/js/arrive.min.js"></script>
<!--  PerfectScrollbar Library -->
<!--<script src="<?php // echo $baseUrl;                   ?>/assets/js/perfect-scrollbar.jquery.min.js"></script>-->
<!--  Notifications Plugin    -->
<script src="<?php echo $baseUrl; ?>/assets/js/bootstrap-notify.js"></script>
<!--  Google Maps Plugin    -->
<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>-->
<!-- Material Dashboard javascript methods -->
<script src="<?php echo $baseUrl; ?>/assets/js/material-dashboard.js?v=1.2.0"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?php echo $baseUrl; ?>/assets/js/demo.js"></script>

<script src="<?php echo $baseUrl; ?>/assets/js/jquery-ui-1.12.1.min.js"></script>

<!--select2-->
<script src="<?php echo $baseUrl; ?>/bower_components/select2/dist/js/select2.min.js"></script>
<!--select2-->

<script src="<?php echo $baseUrl; ?>/assets/js/ImageTools.js"></script>

<script src="<?php echo $baseUrl; ?>/assets/js/custom.js"></script>
<script type="text/javascript">

    var baseUrl = "<?php echo $baseUrl; ?>";

    function push_notificatoin(color, icon, msg) {
        $('.push-notificatoin-container').remove();
        var push_notificatoin = '<div class="push-notificatoin-container ' + color + '"><div class="push-notificatoin-inner"><div class="push-notificatoin-icon"><i class="fa fa-' + icon + '"></i></div><div class="push-notificatoin-msg">' + msg + '</div></div><i class="fa fa-times close-push-notificatoin" onclick="close_push_notificatoin()"></i></div>';
        $('body').prepend(push_notificatoin);
        setTimeout(function () {
            $('.push-notificatoin-container').addClass('after');
            setTimeout(function () {
                $('.push-notificatoin-container').addClass('after1');
                setTimeout(function () {
                    $('.push-notificatoin-container').remove();
                }, 2000);
            }, 4000);
        }, 100);
    }
    function close_push_notificatoin() {
        $('.push-notificatoin-container').addClass('after2');
        setTimeout(function () {
            $('.push-notificatoin-container').remove();
        }, 500);
    }
    function system_error() {
        var color = 'red';
        var icon = 'times';
        var msg = 'Something went wrong! please contact the administrator';
        push_notificatoin(color, icon, msg);
    }
    function start_load() {
        var loading = `
                <div class="loading" id="loading">
                    <div class="loading-inner">
                        <img src="` + baseUrl + `/assets/img/loading.gif"/>
                        <h6>Loading...</h6>
                    </div>
                </div>
                `;
        $('body').append(loading);
    }
    function stop_load() {
        $('#loading').remove();
    }

    function start_upload() {
        var loading = `
                <div class="loading" id="loading">
                    <div class="loading-inner">
                        <img src="` + baseUrl + `/assets/img/loading.gif"/>
                        <h6>Uploading...</h6>
                    </div>
                </div>
                `;
        $('body').append(loading);
    }
    function stop_upload() {
        $('#loading').remove();
    }
    $('.navbar-nav>li').on('click', function () {
        $(this).toggleClass('open');
    });

    if (screen.width < 1200) {
        menu_remover();
    } else {
    }
    if (screen.resize < 1200) {
        menu_remover();
    } else {
    }

    function menu_remover() {
        setTimeout(function () {
            $('body').removeClass("menu-is-active");
            $('body').addClass("menu-is-not-active");
        }, 1000);
    }

//    function requestFullScreen(element) {
//        // Supports most browsers and their versions.
//        var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullScreen;
//
//        if (requestMethod) { // Native full screen.
//            requestMethod.call(element);
//        } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
//            var wscript = new ActiveXObject("WScript.Shell");
//            if (wscript !== null) {
//                wscript.SendKeys("{F11}");
//            }
//        }
//    }
//    var elem = document.body; // Make the body go full screen.
//    requestFullScreen(elem);
    
    var refreshSn = function ()
    {
        var time = 300000; // 5 mins
        settimeout(
                function ()
                {$.ajax({
                        url: 'refresh_session.php',
                        cache: false,
                        complete: function () {
                            refreshSn();
                        }
                    });
                }, time);
    };
</script>
<?php include_once 'access.php'; ?>
</html>