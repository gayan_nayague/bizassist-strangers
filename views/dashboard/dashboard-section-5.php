<?php
include_once '../../controllers/connection.php';
//$branch_loc_id = htmlspecialchars($_GET["branch_location"]);
$branch_loc_id = htmlspecialchars($_GET["branch_loc_id"]);
$from_date = htmlspecialchars($_GET["from_date"]);
$to_date = htmlspecialchars($_GET["to_date"]);


//date_default_timezone_set('Asia/Calcutta');
$today = date("Y-m-d");

$today_strtotime = strtotime($today . ' +2 month');
$today_strtotime_after = date('Y-m-d', $today_strtotime);

$get_delete_log = "
SELECT
d.delete_log_id,
d.time,
d.details,
d.user_id,
u.username,
u.first_name,
u.last_name,
b.branch_location
FROM delete_log d
LEFT OUTER JOIN `user` u
ON u.user_id = d.user_id
LEFT OUTER JOIN branch b
ON b.branch_id = u.branch_id
WHERE d.time BETWEEN '$today' AND '$today_strtotime_after' AND b.branch_id = '$branch_loc_id'
        ";
$result_get_delete_log = $conn->query($get_delete_log);

$get_login_log = "
SELECT
l.login_log_id,
l.user_id,
u.username,
b.branch_location,
l.login_time,
l.logout_time
FROM login_log l
LEFT OUTER JOIN `user` u
ON u.user_id = l.user_id
LEFT OUTER JOIN branch b
ON b.branch_id = u.branch_id
WHERE l.login_time BETWEEN '$today' AND '$today_strtotime_after' AND b.branch_id = '$branch_loc_id'
        ";
$result_get_login_log = $conn->query($get_login_log);
?>
<script>
    $(document).ready(function () {
        $('.details-expand').on('click', function () {
            var details_expand_container = $(this).find('.details-expand-container').text();
            var details_expand_container_result = details_expand_container.replace(/\,/g, '<br/>');
            var details_container = `
            <div class="sale_detail_container">
                <div class="sale_detail_block" style="max-width: 500px">
                    <div class="close-detail-btn">
                        <i class="material-icons" onclick="remove_pop_up()">close</i>
                    </div>
                    <div class="sale_detail_container_loader">` + details_expand_container_result + `</div>
                </div>
            </div>
        `;
            $('body').append(details_container);
        });
    });
    function remove_pop_up() {
        $('.sale_detail_container').remove();
    }
</script>
<!--<div class="row">-->
<h3 class="dash-sector-title">All logs</h3>
<div class="col-lg-6 col-md-12">
    <div class="card card-nav-tabs">
        <div class="card-header" data-background-color="green">
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <!--<span class="nav-tabs-title">Items:</span>-->
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="active">
                            <a href="#delete_log" data-toggle="tab">
                                <i class="material-icons">remove_circle_outline</i> Product delete log
                                <div class="ripple-container"></div>
                            </a>
                        </li>
                        <li class="">
                            <a href="#login_log" data-toggle="tab">
                                <i class="material-icons">account_circle</i> User login log
                                <div class="ripple-container"></div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-content">
            <div class="tab-content">
                <div class="tab-pane active" id="delete_log">
                    <div class="dash-table-scroll">
                        <table class="table table-hover" id="cat-table">
                            <thead class="text-warning">
                            <th><span>#</span></th>
                            <th><span>Time</span></th>
                            <th><span>Details</span></th>
                            <th style="width:100px;"><span>Deleted by</span></th>
                            <th><span>Deleted at</span></th>
                            </thead>
                            <tbody>
                                <?php
                                if ($result_get_delete_log->num_rows > 0) {
                                    $numOrder = 1;
                                    while ($row = $result_get_delete_log->fetch_assoc()) {
                                        ?>
                                        <tr id="<?php echo $row["delete_log_id"]; ?>">
                                            <td style="width: 10%"><?php echo ($numOrder++); ?></td>
                                            <td style="white-space: nowrap;"><?php echo $row["time"]; ?></td>
                                            <td class="details-expand">
                                                <span class="details-expand-container"><?php echo $row["details"]; ?></span>
                                            </td>
                                            <td><?php echo $row["username"]; ?></td>
                                            <td><?php echo $row["branch_location"]; ?></td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    echo "0 results";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <div class="tab-pane" id="login_log">
                    <div class="dash-table-scroll">
                        <table class="table table-hover" id="cat-table">
                            <thead class="text-warning">
                            <th><span>#</span></th>
                            <th><span>Username &nbsp; &nbsp;</span></th>
                            <th style="min-width: 100px;"><span>Branch location &nbsp; &nbsp;</span></th>
                            <th><span>Login time &nbsp; &nbsp;</span></th>
                            <th><span>Logout time &nbsp; &nbsp;</span></th>
                            </thead>
                            <tbody>
                                <?php
                                if ($result_get_login_log->num_rows > 0) {
                                    $numOrder = 1;
                                    while ($row = $result_get_login_log->fetch_assoc()) {
                                        ?>
                                        <tr id="<?php echo $row["delete_log_id"]; ?>">
                                            <td style="width: 10%"><?php echo ($numOrder++); ?></td>
                                            <td><?php echo $row["username"]; ?></td>
                                            <td style="min-width: 100px;"><?php echo $row["branch_location"]; ?></td>
                                            <td style="white-space: nowrap;"><?php echo $row["login_time"]; ?></td>
                                            <td style="white-space: nowrap;"><?php echo $row["logout_time"]; ?></td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    echo "0 results";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<!--</div>-->